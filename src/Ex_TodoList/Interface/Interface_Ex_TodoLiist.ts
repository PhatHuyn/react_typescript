export interface InterfaceTodo{
  id: string;
  text: string;
  isCompleted: boolean;
}

export interface InterTodoListComponent{
  todos:InterfaceTodo[];
  handleTodoRemove:(id:string) => void;
}

export interface InterfaceTodoItemComponent{
  todo: InterfaceTodo;
  handleTodoRemove:(id:string) => void;
}

export interface InterfaceTodoFormComponent{
  handleToDoAdd: (todo:InterfaceTodo) => void;
}