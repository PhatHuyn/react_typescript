import React from 'react'
import { InterTodoListComponent } from '../Interface/Interface_Ex_TodoLiist'
import TodoItem from '../TodoItem/TodoItem';

export default function TotoList({ todos, handleTodoRemove }: InterTodoListComponent) {
  console.log("todos", todos);

  return (
    <div className='container mx-auto'>
      <div className="overflow-x-auto relative">
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="py-3 px-6">
                ID{' '}
              </th>
              <th scope="col" className="py-3 px-6">
                Name
              </th>
              <th scope="col" className="py-3 px-6">
                IsCompleted
              </th>
            </tr>
          </thead>
          <tbody>
            {todos.map((item) => {
              return <TodoItem handleTodoRemove={handleTodoRemove} todo={item} />
            })}
          </tbody>
        </table>
      </div>

    </div>
  )
}
