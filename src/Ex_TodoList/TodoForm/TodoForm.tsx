import { nanoid } from 'nanoid';
import React, { useState } from 'react'
import { InterfaceTodo, InterfaceTodoFormComponent } from '../Interface/Interface_Ex_TodoLiist';

export default function TodoForm({ handleToDoAdd }: InterfaceTodoFormComponent) {
  const [title, setTitle] = useState<string>('');

  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value);
    let value = e.target.value;
    setTitle(value);

  };

  const handleTodoCreate = () => {
    let newTodo: InterfaceTodo = {
      id: nanoid(),
      text: title,
      isCompleted: false,
    }
    handleToDoAdd(newTodo);
    setTitle("");
  };

  return <div className='my-20 flex'>
    <input onChange={handleOnChange} type="text"
      className='p-5 rounded border border-gray-500 flex-grow'
      value={title}
    />
    <button onClick={handleTodoCreate} className='bg-red-500 text-white rounded px-5 py-2 
        flex-shrink-0'>
      Add Todo
    </button>
  </div>;
}
