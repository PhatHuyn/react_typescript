import React, { useState } from 'react'
import { InterfaceTodo, InterfaceTodoFormComponent } from './Interface/Interface_Ex_TodoLiist';
import TodoForm from './TodoForm/TodoForm'
import TotoList from './TotoList/TotoList'



export default function Ex_TodoList() {
    const [todos, setTodos] = useState<InterfaceTodo[]>([
        {
            id: "1",
            text: "Lam du an cuoi khoa",
            isCompleted: false,
        },
        {
            id: "2",
            text: "Lam CapStone React",
            isCompleted: false,
        }
    ]);

    const handleToDoAdd = (todo: InterfaceTodo) => {
        let newTodos = [...todos, todo];
        setTodos(newTodos);
    };
    const handleTodoRemove = (idTodo: string) => {
        let newTodos = todos.filter((todo) => {
            return todo.id !== idTodo;
        });
        setTodos(newTodos);
    }

    return (
        <div>
            <TodoForm handleToDoAdd={handleToDoAdd} />
            <TotoList handleTodoRemove={handleTodoRemove} todos={todos} />
        </div>
    )
}
