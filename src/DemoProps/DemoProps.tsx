import React from 'react'
import UserInfor from './UserInfor/UserInfor'

export interface InterfaceDataUser{
    name:string;
    age:number;
}

let data:InterfaceDataUser = {
    name: "Alice",
    age: 2,
}

export default function DemoProps() {
  return (
    <div>
        <p>DemoProps</p>
        <UserInfor user={data} />
    </div>
  )
}
